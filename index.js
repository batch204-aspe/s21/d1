/*
	Mini Activity
		Display the ff student number in our console.
		
		"2020-1923"
		"2020-1924"
		"2020-1925"
		"2020-1926"
		"2020-1927"

*/

let studNumber = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];
console.log(studNumber);

/*
	Array
		- used to store multiple related values in a single variable
		- declared using square bracket ( [] ) also known as "Array Literals"

	SYNTAX: 
		let/const arrayName = [elementA, elementB .. elementN];

*/

// Common Examples of Arrays
let grades = [98.5, 94.3, 95, 90];
let computerBrands = ["Acer", "Asus", "HP", "Dell", "Lenovo", "Toshiba", "Fujitsu"];

// Possible to used with different data type but it is not recommended 
let mixedArr = [12, "Asus", null, undefined, {} ];

let myTasks = [
				'drink html',
				'eat javascript',
				'inhale css',
				'bake sass'
				];
console.log(myTasks);


// you can also store the declared variables to arrayElement
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Berlin";

let cities = [city1, city2, city3];
console.log(cities);

// Reassigning array values
console.log('Array before reassignment: ');
console.log(myTasks);
myTasks[0] = "Hello World";
console.log('Array after reassignment: ')
console.log(myTasks);

// Array Length Property
// the .length property allows us to get and set the total number of items in array
console.log(myTasks.length); // 4
console.log(cities.length); // 3

let blankArr = [];
console.log(blankArr,length); // 0


let fullName = "Jonathan Nuestro";
console.log(fullName.length); // 16


myTasks.length = myTasks.length - 1; // myTask = 4 -1 = 3
console.log(myTasks.length); 
console.log(myTasks); 

// Another example using decrementation
console.log(cities);
cities.length--;
console.log(cities);

// Can we delete a character in a "String" using .length property
fullName.length = fullName.length - 1;
console.log(fullName.length);
console.log(fullName);


let theBeatles = ["John", "Paul", "Ringo", "George"]
theBeatles.length++;
console.log(theBeatles);

/*
	Accessing Elements of an Array

	Syntax: 
		arrayName[index[]]; // index is always start at 0

*/

console.log(grades[3]); // 90
console.log(computerBrands[2]); // HP
console.log(computerBrands[20]); // return as undefined


let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]
console.log(lakersLegends[1]); // Shaq
console.log(lakersLegends[3]); // Magic

// reassigning the arrayElement to a new Variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

// Reassigning the value of an Element Array
console.log("Array before reassignment");
console.log(lakersLegends);

lakersLegends[2] = "Gasol"; // reassigning the ELement Array
console.log("Array After reassignment");
console.log(lakersLegends);

// Accessing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose"];

let lasteElementIndex = bullsLegends.length - 1;
console.log(bullsLegends[lasteElementIndex]); //Rose
console.log(bullsLegends[bullsLegends.length -1]); // Rose

// Adding Items into the Array
let newArray = [];
console.log(newArray[0]); // this will return undefined

newArray[0] = "Rafael Santillan";
newArray[1] = "Mark Joseph";
console.log(newArray);

// Adding ArrayElement in the last index
newArray[newArray.length] = "Mikki Dela Cerna";
console.log(newArray);

// TO override the last ArrayElement in the last index
newArray[newArray.length - 1] = "Jayson Carlos";
console.log(newArray);

/*
	Looping over an Array

*/

for (let index = 0; index < newArray.length; index++){
	console.log(newArray[index]);
}

let numArr = [49, 14, 30, 12, 22];

for (let index = 0; index < numArr.length; index++) {

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}
	else{
		console.log(numArr[index] + " is not divisible by 5");
	}
}

// Multi-Dimensional Arrays

let chessBoard = [
					['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
					['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
					['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
					['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
					['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
					['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
					['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
					['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
				 ];
console.log(chessBoard);
// Accessing the ELementArray
// multiArrayName [OuterArray][InnerArray]
console.log(chessBoard[4][4]); // e5
console.log("Pawn moves to: " + chessBoard[7][4]); // Pawn moves to: e8
